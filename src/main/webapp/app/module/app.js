'use strict';

/**
 * @ngdoc overview
 * @name MatterhornApp
 * @description
 * # MatterhornApp
 *
 * Main module of the application.
 */
angular.module('MatterhornApp', [ "ui.router", 
                                  "ui.bootstrap", 
                                  "oc.lazyLoad",  
                                  "ngSanitize",
                                  'ngResource',
	                              'com.module.users',
	                              'com.module.core',
                                  'com.module.admin'
                                
]).directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                    Layout.closeMainMenu();
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setMainMenuActiveLink('match'); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]).directive('a',
	    function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
}).directive('dropdownMenuHover', function () {
	  return {
		    link: function (scope, elem) {
		      elem.dropdownHover();
		    }
		  };
}).config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	
    $ocLazyLoadProvider.config({

    });
}]).config(['$controllerProvider', function($controllerProvider) {
	  // this option might be handy for migrating old apps, but please don't use it
	  // in new ones!
	  $controllerProvider.allowGlobals();
}]).config(['$locationProvider', function($locationProvider) {

    $locationProvider.hashPrefix('!');
  

}]).config(['$resourceProvider', function($resourceProvider) {
	  // Don't strip trailing slashes from calculated URLs
	  $resourceProvider.defaults.stripTrailingSlashes = false;

}]).factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: 'assets',
        globalPath: 'assets/global',
        layoutPath: 'assets/layouts/layout3'
    };

    $rootScope.settings = settings;

    return settings;
}]).run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);