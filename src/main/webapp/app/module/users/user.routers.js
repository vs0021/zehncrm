'use strict';

var app = angular.module('com.module.users', []);

app.config(["$stateProvider",  "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
	
	$stateProvider
	
		.state('login', {
			url : '/login',
			templateUrl: 'module/users/views/login.html',

			controller: 'LoginCtrl'
//			resolve: {
//				deps: ['$ocLazyLoad', function ($ocLazyLoad) {
//                    return $ocLazyLoad.load({
//                        name: 'login',
//                        files: [
//                            'app/js/jquery.validate.min.js',
//                            'app/js/login-4.min.js'
//                        ]
//                    });
//                }]
//			}
//			controller: 'LoginCtrl'
		});
	
}]);
