'use strict';

/**
 * @ngdoc overview
 * @name com.module.core
 * @module
 * @description
 * @requires coursekApp
 *
 * The `core` module provides services for interacting with
 * Core module of application
 *
 */

var app = angular.module('com.module.core', []);

app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
	
	$stateProvider
		.state('app', {
			url: '/',
			templateUrl: 'module/core/views/main.html'
		})
		.state('app.dashboard', {
			url: '',
			templateUrl: "module/core/views/dashboard.html",
            data: {pageTitle: 'Admin Dashboard Template'},
			controller: 'DashboardCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MatterhornApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'assets/global/plugins/morris/morris.css',
                            'assets/global/plugins/morris/morris.min.js',
                            'assets/global/plugins/morris/raphael-min.js',
                            'assets/global/plugins/jquery.sparkline.min.js',

                            'assets/pages/scripts/dashboard.min.js',
                            'module/core/controllers/core.ctrl.js',
                        ]
                    });
                }]
            }
		});
	$urlRouterProvider.otherwise('/login');
}]);