'use strict';

angular.module('com.module.core')
//	.factory('settings', settings)
	.controller('AppController',AppController)
	.controller('HeaderController',HeaderController)
	.controller('SidebarController',SidebarController)
	.controller('QuickSidebarController',QuickSidebarController)
	.controller('PageHeadController',PageHeadController)
	.controller('ThemePanelController',ThemePanelController)
	.controller('FooterController',FooterController)
//	.controller('HeaderCtrl',HeaderCtrl)
//	.controller('FooterCtrl',FooterCtrl)
    .controller('DashboardCtrl', DashboardCtrl);

//settings.$inject = ["$rootScope"];
AppController.$inject = ["$scope", "$rootScope"];
HeaderController.$inject = ["$scope"];
SidebarController.$inject = ["$scope"];
QuickSidebarController.$inject = ["$scope"];
PageHeadController.$inject = ["$scope"];
ThemePanelController.$inject = ["$scope"];
FooterController.$inject = ["$scope"];
//HeaderCtrl.$inject =["$scope","$resource"];
//FooterCtrl.$inject =["$scope","$resource"];
DashboardCtrl.$inject = ["$rootScope", "$scope","$resource", "http", "timeout"];

//function settings($rootScope) {
//	// supported languages
//    var settings = {
//        layout: {
//            pageSidebarClosed: false, // sidebar menu state
//            pageContentWhite: true, // set page content layout
//            pageBodySolid: false, // solid body color state
//            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
//        }/*,
//        assetsPath: '../assets',
//        globalPath: '../assets/global',
//        layoutPath: '../assets/layouts/layout3'*/
//    };
//
//    $rootScope.settings = settings;
//
//    return settings;
//}

function AppController($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });
}

function HeaderController($scope) {
	
	$scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });

}

function SidebarController($scope) {
	
	$scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });

}

function QuickSidebarController($scope) {
	
	$scope.$on('$includeContentLoaded', function() {
	       setTimeout(function(){
	            QuickSidebar.init(); // init quick sidebar        
	        }, 2000)
	    });

}

function PageHeadController($scope) {
	
	$scope.$on('$includeContentLoaded', function() {        
        Demo.init(); // init theme panel
    });

}

function ThemePanelController($scope) {
	
	$scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });

}

function FooterController($scope,$resource,$http) {
	
	$scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });

}
//
//function FooterCtrl($scope,$resource,$http) {
//	
//	console.log("FooterCtrl");
//
//}

function DashboardCtrl($rootScope, $scope, $resource, $http, $timeout) {

	console.log("DashboardCtrl");

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

}