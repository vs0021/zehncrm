'use strict';

/**
 * @ngdoc overview
 * @name com.module.admin
 * @module
 * @description
 * @requires coursekApp
 *
 * The `admin` module provides services for interacting with
 * Admin module of application
 *
 */

var app = angular.module('com.module.admin', []);

app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('app.setup', {
            url: 'setup',
            templateUrl: "module/admin/views/admin-main.html",
            data: {pageTitle: 'Setup'},
			controller: 'UserCtrl',
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MatterhornApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            'assets/pages/css/profile.css',

                            'assets/global/plugins/jquery.sparkline.min.js',
                            'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                            'assets/pages/scripts/profile.min.js'

                            //'js/controllers/UserProfileController.js'
                        ]
                    });
                }]
            }
        })
        .state('app.setup.contects', {
            url: '/contects',
            templateUrl: "module/admin/views/user-list.html",
			controller: 'UserCtrl'
        })
}]);