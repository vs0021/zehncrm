package com.zehntech.matterhorn.domain.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "USER_REGISTRATION")
public class UserRegistration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_registration_id")
	private int Id;

	@Column(name = "resetlink")
	private String resetLink;

	@Column(name = "validFlag")
	private int validFlag;

	@Column(name = "expiry_date")
	private Date expiryDate;

	@Column(name = "generate_date")
	private Date generateDate;

	@OneToOne(cascade = { CascadeType.ALL })
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "auth_user_id")
	private AuthUsersData authUsersData;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getResetLink() {
		return resetLink;
	}

	public void setResetLink(String resetLink) {
		this.resetLink = resetLink;
	}

	public int getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(int validFlag) {
		this.validFlag = validFlag;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getGenerateDate() {
		return generateDate;
	}

	public void setGenerateDate(Date generateDate) {
		this.generateDate = generateDate;
	}

	public AuthUsersData getAuthUsersData() {
		return authUsersData;
	}

	public void setAuthUsersData(AuthUsersData authUsersData) {
		this.authUsersData = authUsersData;
	}

}
