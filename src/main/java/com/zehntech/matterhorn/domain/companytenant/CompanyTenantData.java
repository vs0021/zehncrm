package com.zehntech.matterhorn.domain.companytenant;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.security.core.SpringSecurityCoreVersion;

import com.zehntech.matterhorn.domain.user.AuthUsersData;

@Entity
@Table(name = "COMPANY_TENANT")
public class CompanyTenantData {

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "company_tenant_id")
	private int id;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "web_site")
	private String webSite;

	@Column(name = "country_Name")
	private String countryName;

	@Column(name = "country_Code")
	private String countryCode;

	@Column(name = "zipcode")
	private String zipcode;

	@Column(name = "address")
	private String address;

	@Column(name = "state_Name")
	private String stateName;

	@Column(name = "state_code")
	private String stateCode;

	@Column(name = "city")
	private String city;

	@Column(name = "number_of_user")
	private int numberOfUser;

	@Column(name = "remaining_user")
	private int remainingUser;

	@Column(name = "expiry_date")
	private Timestamp expiryDate;

	@Column(name = "status")
	private boolean status;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "companyTenantData")
	@NotFound(action = NotFoundAction.IGNORE)
	private List<AuthUsersData> authUsersData = new ArrayList<AuthUsersData>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<AuthUsersData> getAuthUsersData() {
		return authUsersData;
	}

	public void setAuthUsersData(List<AuthUsersData> authUsersData) {
		this.authUsersData = authUsersData;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getNumberOfUser() {
		return numberOfUser;
	}

	public void setNumberOfUser(int numberOfUser) {
		this.numberOfUser = numberOfUser;
	}

	public Timestamp getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}

	public int getRemainingUser() {
		return remainingUser;
	}

	public void setRemainingUser(int remainingUser) {
		this.remainingUser = remainingUser;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}
