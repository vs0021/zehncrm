/*
 Name                    Date             version                     Description
Akhilesh Prajapati      21/Feb/2014        1.1                        This is the Class to use hashing the file using SHA-256 algorithm.
 */

package com.zehntech.matterhorn.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;

import com.zehntech.matterhorn.logging.ApplicationLogger;

@Component("hashing")
public class Hashing {

	ApplicationLogger logger = ApplicationLogger.getLogger(Hashing.class);

	MessageDigest digest = null;

	String convertedName = null;

	public String SHA256Hashing(String folderName) {

		try {
			digest = MessageDigest.getInstance("SHA-256");
		}
		catch (NoSuchAlgorithmException e) {
			logger.debug("Exception in SHA256Hashing:" + e);

		}
		byte[] convertByte = digest.digest(folderName.getBytes());

		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < convertByte.length; i++) {
			buffer.append(Integer.toString((convertByte[i] & 0xFF) + 0x100, 32)
					.substring(1));
			convertedName = buffer.toString();

		}
		logger.debug(folderName.toString() + " is change in to hashed name ");

		return convertedName;
	}

	public String SHA256Hashing16Bit(String password) {

		try {
			digest = MessageDigest.getInstance("SHA-256");
		}
		catch (NoSuchAlgorithmException e) {
			logger.error("Exception in SHA256Hashing:" + e);
		}
		byte[] convertByte = digest.digest(password.getBytes());

		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < convertByte.length; i++) {
			buffer.append(Integer.toString((convertByte[i] & 0xFF) + 0x100, 16)
					.substring(1));
			convertedName = buffer.toString();

		}
		logger.debug(password.toString() + " is change in to hashed name ");

		return convertedName;
	}

}
