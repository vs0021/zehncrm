package com.zehntech.matterhorn.utility;

import java.util.Calendar;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zehntech.matterhorn.bean.forms.CompanyAdminDetailForm;
import com.zehntech.matterhorn.bean.forms.UserDetailForm;
import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.domain.user.AuthUsersData;
import com.zehntech.matterhorn.domain.user.UserRegistration;
import com.zehntech.matterhorn.logging.ApplicationLogger;

@Component
public class UserUtil {
	ApplicationLogger logger = ApplicationLogger.getLogger(UserUtil.class);

	@Autowired
	DateUtility dateUtility;

	public CompanyTenantData populateCompanyTenantData(
			CompanyAdminDetailForm companyAdminDetailForm) {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 15);
		CompanyTenantData companyTenantData = new CompanyTenantData();
		companyTenantData.setCompanyName(companyAdminDetailForm
				.getCompanyName());
		companyTenantData.setStatus(true);
		companyTenantData.setExpiryDate(new java.sql.Timestamp(calendar
				.getTime().getTime()));
		/*
		 * companyTenantData.setAddress(companyTenantForm.getAddress());
		 * companyTenantData.setCity(companyTenantForm.getCity());
		 * companyTenantData.setCountryName(companyTenantForm.getCountryName());
		 * companyTenantData.setExpiryDate(dateUtility
		 * .stringToTimeStamp(companyTenantForm.getExpiryDate()));
		 * companyTenantData.setStateName(companyTenantForm.getStateName());
		 * companyTenantData.setWebSite(companyTenantForm.getWebSite());
		 * companyTenantData.setZipcode(companyTenantForm.getZipCode());
		 * 
		 * companyTenantData.setNumberOfUser(companyTenantForm.getNumberOfUser())
		 * ; companyTenantData.setStatus(companyTenantForm.isStatus());
		 * companyTenantData.setCountryCode(companyTenantForm.getCountryCode());
		 * companyTenantData.setStateCode(companyTenantForm.getStateCode()); if
		 * (companyTenantForm.getCompanyTenantId() == 0) {
		 * companyTenantData.setRemainingUser(companyTenantForm
		 * .getNumberOfUser()
		 * 
		 * ); } else {
		 * companyTenantData.setId(companyTenantForm.getCompanyTenantId());
		 * companyTenantData.setRemainingUser(companyTenantForm
		 * .getRemainingUser()); }
		 */
		return companyTenantData;

	}

	public AuthUsersData populateAuthUsersData(UserDetailForm userDetailForm) {
		AuthUsersData authUsersData = new AuthUsersData();
		Boolean enabled = true;
		UserRegistration userRegistration = new UserRegistration();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 3);
		try {

			authUsersData.setUserName(userDetailForm.getUserName());

			authUsersData.setPassword(userDetailForm.getPassword());
			authUsersData.setPhoneNumber(userDetailForm.getPhoneNumber());
			authUsersData.setEnabled(enabled);
			authUsersData.setLoginId(userDetailForm.getLogin());
			userRegistration.setResetLink(UUID.randomUUID().toString());
			userRegistration.setGenerateDate(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			/*
			 * userRegistration.setExpiryDate(new java.sql.Timestamp(calendar
			 * .getTime().getTime()));
			 */
			userRegistration.setValidFlag(0);
			userRegistration.setAuthUsersData(authUsersData);
			authUsersData.setUserRegistration(userRegistration);
		}
		catch (Exception e) {
			logger.info("Exception in populateAuthUsersData method"
					+ e.getMessage());

		}

		return authUsersData;

	}
}
