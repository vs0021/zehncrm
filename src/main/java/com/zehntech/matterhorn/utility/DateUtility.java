/**
 * 
 */

package com.zehntech.matterhorn.utility;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.zehntech.matterhorn.logging.ApplicationLogger;

/**
 * 
 * @author akhileshprajapati
 * 
 */
@Component
public class DateUtility {

	private static final String dateFormatWithoutTime = "dd-MM-yyyy";

	private static final String dateFormatWithTime = "dd-MM-yyyy HH:mm";

	private static final String startMin = "00";

	private static final String timeSeparator = ":";

	private static final String startHour = "00";

	private static final String endHour = "23";

	private static final String endMin = "59";

	private static ApplicationLogger logger = ApplicationLogger
			.getLogger(DateUtility.class);

	/**
	 * This method will convert JAVA date into String
	 * 
	 * @param date
	 * @return
	 */

	public static String dateToString(Date date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(dateFormatWithTime);
		String sdate = dateFormat.format(date);
		return sdate;
	}

	/**
	 * This method will convert JAVA date into String
	 * 
	 * @param date
	 * @return
	 */

	public static String emailDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm a");
		String sdate = dateFormat.format(date);
		return sdate;
	}

	/**
	 * This method will convert JAVA date into String
	 * 
	 * @param date
	 * @return
	 */

	public static String dateToDateString(Date date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(dateFormatWithTime);
		String sdate = dateFormat.format(date);
		return sdate;
	}

	/**
	 * This method will convert JAVA date into SQL date
	 * 
	 * @param date
	 * @return
	 */

	public static java.sql.Date dateToSQLDate(java.util.Date date) {
		if (date == null) {
			return null;
		}
		else {
			return new java.sql.Date(date.getTime());
		}
	}

	/**
	 * This method will convert SQL date into JAVA date
	 * 
	 * @param date
	 * @return
	 */
	public static java.util.Date SQLDateToDate(java.sql.Date date) {
		if (date == null) {
			return null;
		}
		else {
			return new java.util.Date(date.getTime());
		}
	}

	/**
	 * This method will convert String into JAVA date
	 * 
	 * @param date
	 * @return
	 */
	public static Date stringToDate(String date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(dateFormatWithTime);
		Date convertDate = null;
		try {
			convertDate = (Date) dateFormat.parse(date);
		}
		catch (ParseException ex) {
		}
		return convertDate;
	}

	/**
	 * This method will convert JAVA date into String to save in database
	 * 
	 * @param date
	 * @return
	 */
	public static String dateToStringInDataBase(Date date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(dateFormatWithTime);
		String stringDate = dateFormat.format(date);
		return stringDate;
	}

	/**
	 * 
	 * @param time
	 * @return
	 */
	public static Object[] timeToString(Date time) {
		if (time != null) {
			DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
			String timeString = dateFormat.format(time);

			return new Object[] { timeString.substring(0, 2),
					timeString.substring(3, 5), timeString.substring(6) };
		}
		else {
			return new Object[] { null, null, null };
		}

	}

	/**
	 * This method will convert String into JAVA time
	 * 
	 * @param time
	 * @return
	 */

	public static Date stringToTime(String time) {
		if (time == null) {
			return null;

		}
		else {
			DateFormat dateFormat = new SimpleDateFormat(dateFormatWithTime);
			Date convertDate = null;
			try {
				convertDate = (Date) dateFormat.parse(time);
			}
			catch (ParseException ex) {
			}
			return convertDate;
		}
	}

	/**
	 * This method will convert timeStamp to String
	 * 
	 * @param timestamp
	 * @return
	 */

	public static String timeStampToString(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}
		else {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					dateFormatWithTime);
			String timeStampString = dateFormat.format(timestamp);
			return timeStampString;
		}
	}

	public static String timeStampToStringMMMDDYYYY(Timestamp timestamp) {
		if (timestamp == null) {
			return null;
		}
		else {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"EEE MMM dd, yyyy hh:mm a");
			String timeStampString = dateFormat.format(timestamp);
			return timeStampString;
		}
	}

	/**
	 * This method will convert String to timeStamp
	 * 
	 * @param timeStamp
	 * @return
	 */

	public static Timestamp stringToTimeStamp(String timeStampString) {
		if (timeStampString == null) {
			return null;
		}
		else {

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					dateFormatWithTime);
			java.util.Date date = null;
			try {
				date = dateFormat.parse(timeStampString);
			}
			catch (ParseException e) {
				e.getMessage();

			}
			java.sql.Timestamp timestamp = new java.sql.Timestamp(
					date.getTime());

			return timestamp;
		}
	}

	public static Timestamp stringToTimeStampDDMMYYYY(String timeStampString) {
		if (timeStampString == null) {
			return null;
		}
		else {

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			java.util.Date date = null;
			try {
				date = dateFormat.parse(timeStampString);
			}
			catch (ParseException e) {
				e.getMessage();

			}
			java.sql.Timestamp timestamp = new java.sql.Timestamp(
					date.getTime());

			return timestamp;
		}
	}

	/**
	 * This method will gives time duration between start date and end date
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Object[] calculateDuration(Date startDate, Date endDate) {

		if (startDate != null && endDate != null) {
			Calendar start = Calendar.getInstance();
			Calendar end = Calendar.getInstance();
			start.setTime(startDate);
			end.setTime(endDate);
			int year = end.get(Calendar.YEAR) - start.get(Calendar.YEAR);
			int month = end.get(Calendar.MONTH) - start.get(Calendar.MONTH);
			int day = Math.abs(end.get(Calendar.DAY_OF_MONTH)
					- start.get(Calendar.DAY_OF_MONTH));

			if (year <= 0) {
				year = 0;
			}

			if (month < 0) {
				month = 0;
			}
			else {
				month += 1;
			}
			if (day <= 0) {
				day = 0;
			}
			return new Object[] { year, month, day };
		}
		else {
			return new Object[] { null, null, null };
		}

	}

	public static void main(String[] args) {

		DateUtility.calculateElapsedTime(
				DateUtility.stringToDate("10-01-2015 21:10:11:110"),
				DateUtility.stringToDate("10-01-2015 21:10:12:330"));

	}

	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return elapsed time between startDate and endDate
	 */

	public static String calculateElapsedTime(Date startDate, Date endDate) {
		String ellapsedTime = null;
		String elapsedHours;
		String elapsedMinutes = null;
		String elapsedSeconds = null;

		long different = (endDate.getTime() - startDate.getTime());
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;

		double h = (double) different / hoursInMilli;

		double minutes = 0;
		if (h >= 1) {
			elapsedHours = String.valueOf(h).substring(0,
					String.valueOf(h).indexOf("."));
			minutes = Double
					.valueOf(String.valueOf(h).substring(
							String.valueOf(h).indexOf("."),
							String.valueOf(h).length())) * 60;
		}
		else {
			elapsedHours = "0";
			minutes = Double.valueOf(String.valueOf(h)) * 60;
		}

		double seconds = 0;
		if (minutes >= 1) {

			elapsedMinutes = String.valueOf(minutes).substring(0,
					String.valueOf(minutes).indexOf("."));
			seconds = Double.valueOf(String.valueOf(minutes).substring(
					String.valueOf(minutes).indexOf("."),
					String.valueOf(minutes).length())) * 60;
		}
		else {

			elapsedMinutes = "0";
			seconds = Double.valueOf(String.valueOf(minutes)) * 60;
		}

		double milliSeconds = 0;
		if (seconds >= 1) {

			elapsedSeconds = String.valueOf(seconds).substring(0,
					String.valueOf(seconds).indexOf("."));
			milliSeconds = Double.valueOf(String.valueOf(seconds).substring(
					String.valueOf(seconds).indexOf("."),
					String.valueOf(seconds).length())) * 1000;
		}
		else {

			elapsedSeconds = "0";
			milliSeconds = Double.valueOf(String.valueOf(seconds)) * 1000;
		}
		int milliSecond = (int) milliSeconds;
		ellapsedTime = elapsedHours + ":" + elapsedMinutes + ":"
				+ elapsedSeconds + ":" + milliSecond;
		// System.out.println("ellapsedTime " + ellapsedTime);
		return ellapsedTime;

	}

	public static Timestamp dateToTimestamp(Date date) {
		Timestamp timestamp = null;
		if (date == null) {
			return null;
		}
		else {
			timestamp = new java.sql.Timestamp(date.getTime());
			return timestamp;

		}
	}

	public static String monthInTimeStamp(int month) {

		switch (month) {
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";
		}
		return null;

	}

	public static int getDays(Date d1, Date d2) {
		int days = 0;
		long difference = d2.getTime() - d1.getTime();
		long differenceDays = difference / (24 * 60 * 60 * 1000) + 1;
		days = (int) differenceDays;
		return days;
	}

	public static Date addStartTimeSuffix(String dateStr) {
		logger.debug("Checking for time suffix");
		SimpleDateFormat dateFormat = null;
		Date date = null;
		if (dateStr == null) {
			logger.debug("Date found to be null");
			return null;
		}
		else {
			try {
				dateFormat = new SimpleDateFormat(dateFormatWithTime);
				date = dateFormat.parse(dateStr);
				logger.debug("Date found to be in foramt " + dateFormatWithTime);

				return date;

			}
			catch (ParseException e) {

			}
			try {
				dateFormat = new SimpleDateFormat(dateFormatWithoutTime);
				dateFormat.parse(dateStr);
				logger.debug("Date found to be in foramt "
						+ dateFormatWithoutTime);
				logger.debug("Adding suffix " + startMin + timeSeparator
						+ startHour);
				dateFormat = new SimpleDateFormat(dateFormatWithTime);
				date = dateFormat.parse(dateStr + " " + startMin
						+ timeSeparator + startHour);
				return date;
			}
			catch (Exception e) {
				logger.debug("Date found to be invalid");
				return null;
			}

		}

	}

	public static Date addEndTimeSuffix(String dateStr) {
		logger.debug("Checking for time suffix");
		SimpleDateFormat dateFormat = null;
		Date date = null;
		if (dateStr == null) {
			logger.debug("Date found to be null");
			return null;
		}
		else {
			try {

				dateFormat = new SimpleDateFormat(dateFormatWithTime);
				date = dateFormat.parse(dateStr);
				logger.debug("Date fount to be in foramt " + dateFormatWithTime);

				return date;

			}
			catch (ParseException e) {

			}
			try {
				dateFormat = new SimpleDateFormat(dateFormatWithoutTime);
				dateFormat.parse(dateStr);
				logger.debug("Date fount to be in foramt "
						+ dateFormatWithoutTime);
				logger.debug("Adding suffix " + endMin + timeSeparator
						+ endHour);
				dateFormat = new SimpleDateFormat(dateFormatWithTime);
				date = dateFormat.parse(dateStr + " " + endMin + timeSeparator
						+ endHour);
				return date;
			}
			catch (Exception e) {
				logger.debug("Date found to be invalid");
				return null;
			}

		}

	}

	public static String DateToStringYYYYMMDDHHmm(Date date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		String sdate = dateFormat.format(date);
		return sdate;
	}

	public static String DateToStringYYYYMMDDHHmmssSS(long date) {
		if (date == 0) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SS");
		String sdate = dateFormat.format(new Date(date));
		return sdate;
	}

	public static Date timestampToDate(Timestamp from) {
		if (from == null) {
			return null;
		}
		Date date = new Date(from.getTime());
		return date;
	}

}
