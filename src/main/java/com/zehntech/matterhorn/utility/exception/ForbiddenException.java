package com.zehntech.matterhorn.utility.exception;

public class ForbiddenException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public ForbiddenException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "ForbiddenException [message=" + message + "]";
	}

}
