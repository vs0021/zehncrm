package com.zehntech.matterhorn.utility.exception;

public class PermissionDeniedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public PermissionDeniedException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String toString() {
		return "PermissionDeniedException [message=" + message + "]";
	}

}
