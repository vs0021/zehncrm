package com.zehntech.matterhorn.utility.exception;

public class NoSpaceAvailableException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public NoSpaceAvailableException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "NoSpaceAvailableException [message=" + message + "]";
	}

}
