package com.zehntech.matterhorn.utility.exception;

public class ConflictException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public ConflictException(String message){
		
		this.message = message;
	}
	
	@Override
	public String toString() {
		
		return "ConflictException [message=" + message + "]";
	}
}
