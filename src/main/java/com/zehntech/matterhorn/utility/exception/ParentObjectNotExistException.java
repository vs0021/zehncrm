package com.zehntech.matterhorn.utility.exception;

public class ParentObjectNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public ParentObjectNotExistException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "ParentObjectNotExist [message=" + message + "]";
	}

}
