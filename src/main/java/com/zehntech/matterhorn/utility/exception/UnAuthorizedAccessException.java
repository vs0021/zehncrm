package com.zehntech.matterhorn.utility.exception;

public class UnAuthorizedAccessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public UnAuthorizedAccessException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "UnAuthorizedAccessException [message=" + message + "]";
	}

}
