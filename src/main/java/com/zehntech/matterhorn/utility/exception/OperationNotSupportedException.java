package com.zehntech.matterhorn.utility.exception;

public class OperationNotSupportedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public OperationNotSupportedException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "OperationNotSupportedException [message=" + message + "]";
	}

}
