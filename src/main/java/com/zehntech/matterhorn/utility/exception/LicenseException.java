package com.zehntech.matterhorn.utility.exception;

public class LicenseException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public LicenseException(String message){
		
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "LicenseException [message=" + message + "]";
	}

}
