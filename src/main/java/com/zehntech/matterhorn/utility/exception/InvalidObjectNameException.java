package com.zehntech.matterhorn.utility.exception;

public class InvalidObjectNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public InvalidObjectNameException(String message) {

		this.message = message;
	}

	@Override
	public String toString() {
		return "InvalidObjectNameException [message=" + message + "]";
	}
}
