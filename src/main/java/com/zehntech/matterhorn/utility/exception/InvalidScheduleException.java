package com.zehntech.matterhorn.utility.exception;

public class InvalidScheduleException extends Exception {

	private static final long serialVersionUID = 1L;

	private String message = null;

	public InvalidScheduleException(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Invalid schedule : " + message;
	}
}
