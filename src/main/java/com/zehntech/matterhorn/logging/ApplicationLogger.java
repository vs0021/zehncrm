package com.zehntech.matterhorn.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;

public class ApplicationLogger {

	private Logger logger;

	public static ApplicationLogger getLogger(String name) {
		Logger newLogger = (Logger) LogManager.getContext(false)
				.getLogger(name);
		return new ApplicationLogger(newLogger);
	}

	private ApplicationLogger(Logger logger) {
		this.logger = logger;
	}

	public void debug(Object message) {
		logger.debug(message);
	}

	public void debug(Object message, Throwable t) {
		logger.debug(message, t);
	}

	public void info(Object message) {
		logger.info(message);
	}

	public void info(Object message, Throwable t) {
		logger.info(message, t);
	}

	public void error(Object message) {
		logger.error(message);
	}

	public void error(Object message, Throwable t) {
		logger.error(message, t);
	}

	public void warn(Object message) {
		logger.warn(message);
	}

	public void addAppender(Appender appender) {

		logger.addAppender(appender);
		logger.getContext().getConfiguration()
				.getLoggerConfig("com.zehntech.flums.AuditLogger")
				.setAdditive(false);
	}

	public void removeAppender(String name) {

		(logger).removeAppender((logger).getAppenders().get(name));

		(logger).getContext().getConfiguration().getAppenders().remove(name);
		(logger).getContext().getConfiguration()
				.getLoggerConfig("com.zehntech.flums.AuditLogger")
				.removeAppender(name);

		(logger).getAppenders().remove(name);
		(logger).getContext().updateLoggers(
				(logger).getContext().getConfiguration());

	}

	public Appender getAppender(String name) {
		return logger.getContext().getConfiguration().getAppender(name);

	}

	public static ApplicationLogger getLogger(Class clazz) {
		Logger newLogger = (Logger) LogManager.getLogger(clazz);
		return new ApplicationLogger(newLogger);

	}
}