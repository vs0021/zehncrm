package com.zehntech.matterhorn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zehntech.matterhorn.logging.ApplicationLogger;

@Controller
public class LoginController {
	ApplicationLogger logger = ApplicationLogger
			.getLogger(LoginController.class);

	public LoginController() {
		System.out.println("LoginController");
		logger.info("LoginController");
	}

	@RequestMapping("/")
	public String logout() {
		return "index";

	}
}
