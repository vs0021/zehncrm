package com.zehntech.matterhorn.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.zehntech.matterhorn.bean.forms.CompanyAdminDetailForm;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import com.zehntech.matterhorn.service.user.UserService;
import com.zehntech.matterhorn.utility.exception.ObjectNotFound;
import com.zehntech.matterhorn.utility.exception.UnAuthorizedAccessException;
import com.zehntech.matterhorn.utility.validations.RestValidation;

@Controller
@RequestMapping("/rest/users")
public class AdminRestController {
	ApplicationLogger logger = ApplicationLogger
			.getLogger(AdminRestController.class);

	@Autowired
	private RestValidation restValidation;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/addtenant", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	void addCompanyTenantUser(@RequestBody
	CompanyAdminDetailForm companyAdminDetailForm) {
		String currentUserName = null;
		HttpServletResponse response = null;
		System.out.println("addCompanyTenantUser::" + companyAdminDetailForm);
		try {

			restValidation.checkNullValueValidation(response,
					companyAdminDetailForm.getLogin(),
					companyAdminDetailForm.getUserName());
			restValidation.emailPatternMatch(companyAdminDetailForm.getLogin());

			userService.addCompanyTenant(companyAdminDetailForm);

		}
		catch (ObjectNotFound e) {
			logger.error("Add CompanyTenant operation is failed " + e);
			response.setStatus(404);
		}
		catch (UnAuthorizedAccessException e) {
			logger.error("Add CompanyTenant operation is failed " + e);
			response.setStatus(401);
		}
		catch (Exception e) {
			logger.error("Add CompanyTenant operation is failed " + e);
			e.printStackTrace();
			response.setStatus(400);
		}

	}

	@RequestMapping(value = "/addtenant1", method = RequestMethod.POST)
	public @ResponseBody
	void addCompanyTenantUser1(HttpServletResponse response, @RequestBody
	String value) {
		String currentUserName = null;

		System.out.println("hey" + value);

	}
}
