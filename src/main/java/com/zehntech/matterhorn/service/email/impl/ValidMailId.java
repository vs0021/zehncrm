package com.zehntech.matterhorn.service.email.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zehntech.matterhorn.logging.ApplicationLogger;

public class ValidMailId {
	public static boolean isValid(String mail) {
		ApplicationLogger logger = ApplicationLogger
				.getLogger(EmailService.class);
		String regex = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
		Matcher matcher;
		Pattern pattern;
		pattern = Pattern.compile(regex);
		matcher = pattern.matcher(mail);
		if (matcher.matches()) {
			logger.info("Your email address is valid.");
			return true;

		}
		return false;
	}

}
