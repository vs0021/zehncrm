/*
 Name                    Date               version                     Description
 soagrawa               13/March/2014        1.0                        This class provide utility method for sending mail.                   
 */
package com.zehntech.matterhorn.service.email.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.smtp.SMTPMessage;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import com.zehntech.matterhorn.service.email.bean.EmailBean;

public class EmailService {
	public Session session;

	static ApplicationLogger logger = ApplicationLogger
			.getLogger(EmailService.class);

	public EmailBean emailBean = null;

	private Properties props;

	private static final String MAIL_USERNAME = "mail.username";

	private static final String MAIL_PASSWORD = "mail.password";

	private String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";

	private String MAIL_STORE_PROTOCOL = "mail.store.protocol";

	private String MAIL_HOST = "mail.host";

	private String username;

	private String password;

	private String host;

	private String transferProtocol;

	private EmailUtil emailUtil = null;

	public EmailService() {

	}

	public EmailService(EmailBean emailbean) {
		this.emailBean = emailbean;
		props = new Properties();
		InputStream input = null;
		try {
			input = getClass().getResourceAsStream("/email.properties");
			props.load(input);
		}
		catch (IOException e) {
			
		}
		username = props.getProperty(MAIL_USERNAME);
		password = props.getProperty(MAIL_PASSWORD);
		host = props.getProperty(MAIL_HOST);
		transferProtocol = props.getProperty(MAIL_TRANSPORT_PROTOCOL);
		logger.info("MAIL_USERNAME" + props.getProperty(MAIL_USERNAME));
		logger.info("MAIL_PASSWORD" + props.getProperty(MAIL_PASSWORD));
		emailUtil = new EmailUtil(props);
	}

	/**
	 * Parse addresses from a comma (and space) separated string into the proper
	 * array
	 */
	public InternetAddress[] parseAddresses(String addresses)
			throws AddressException {
		List<InternetAddress> list = new ArrayList<InternetAddress>();
		list.clear();
		StringTokenizer st = new StringTokenizer(addresses, ", ");
		while (st.hasMoreTokens()) {
			list.add(new InternetAddress(st.nextToken()));
		}
		return list.toArray(new InternetAddress[list.size()]);
	}

	/**
	 * Utility method for setting addresses
	 * 
	 * @return
	 * @throws SessionCreationException
	 */

	public SMTPMessage setAddress() throws SessionCreationException {
		try {

			session = emailUtil.getSession(username, password);

			SMTPMessage msg = new SMTPMessage(session);
			if (emailBean.getFrom() == null) {
			}
			else {

				msg.setFrom(new InternetAddress(emailBean.getFrom()));
				logger.debug("email send from  " + emailBean.getFrom());

			}
			if (emailBean.getTo() == null) {
			}
			else {
				InternetAddress[] toAddress = parseAddresses(emailBean.getTo());
				msg.setRecipients(Message.RecipientType.TO, toAddress);
				logger.info("email successfully sent at \""+emailBean.getTo()+"\"");
			}
			if (emailBean.getCc() == null) {
			}
			else {
				InternetAddress[] ccAddress = parseAddresses(emailBean.getCc());
				msg.setRecipients(Message.RecipientType.CC, ccAddress);
				logger.info("cc address is  " + emailBean.getCc());
			}
			if (emailBean.getBcc() == null) {
			}
			else {
				InternetAddress[] bccAddress = parseAddresses(emailBean
						.getBcc());
				msg.setRecipients(Message.RecipientType.BCC, bccAddress);
				logger.info("Bcc address is  " + emailBean.getBcc());
			}

			InternetAddress[] replyTo = parseAddresses(emailBean.getFrom());
			msg.setReplyTo(replyTo);
			msg.setSentDate(new Date());
			msg.setSubject(emailBean.getSubject());
			msg.setReturnOption(SMTPMessage.RETURN_FULL);
			msg.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
			msg.setNotifyOptions(SMTPMessage.NOTIFY_FAILURE);
			return msg;
		}
		catch (MessagingException e) {
			try {
				logger.error("Address cannot be null.");
				throw new EmailExceptions("Address cannot be null.");

			}
			catch (EmailExceptions e1) {

			}
		}
		return null;
	}

	public SMTPMessage setAddress(EmailBean emailBean)
			throws SessionCreationException {
		try {

			session = getSession(emailBean);

			SMTPMessage msg = new SMTPMessage(session);
			if (emailBean.getFrom() != null) {

				msg.setFrom(new InternetAddress(emailBean.getFrom()));
				logger.debug("email send from  " + emailBean.getFrom());

			}
			if (emailBean.getTo() != null) {

				InternetAddress[] toAddress = parseAddresses(emailBean.getTo());
				msg.setRecipients(Message.RecipientType.TO, toAddress);
				logger.info("email successfully sent at \""+emailBean.getTo()+"\"");
			}
			if (emailBean.getCc() != null) {

				InternetAddress[] ccAddress = parseAddresses(emailBean.getCc());
				msg.setRecipients(Message.RecipientType.CC, ccAddress);
				logger.info("cc address is  " + emailBean.getCc());
			}
			if (emailBean.getBcc() != null) {
				InternetAddress[] bccAddress = parseAddresses(emailBean
						.getBcc());
				msg.setRecipients(Message.RecipientType.BCC, bccAddress);
				logger.info("Bcc address is  " + emailBean.getBcc());
			}
			if (emailBean.getReplyTo() != null) {
				InternetAddress[] replyTo = parseAddresses(emailBean
						.getReplyTo());
				msg.setReplyTo(replyTo);
				logger.debug("ReplyTo address is  " + emailBean.getReplyTo());
			}
			msg.setSentDate(new Date());
			msg.setSubject(emailBean.getSubject());
			msg.setReturnOption(SMTPMessage.RETURN_FULL);
			msg.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
			msg.setNotifyOptions(SMTPMessage.NOTIFY_FAILURE);
			return msg;
		}
		catch (MessagingException e) {
			try {
				logger.error("Address cannot be null.");
				throw new EmailExceptions("Address cannot be null.");

			}
			catch (EmailExceptions e1) {

			}
		}
		return null;
	}

	/**
	 * This method send the email to specific mail address
	 * @param msg
	 * @throws MessagingException
	 */
	public void sendMail(SMTPMessage msg) throws MessagingException {

		Transport transport;
		if (session != null)
			transport = session.getTransport(transferProtocol);
		transport = session.getTransport(transferProtocol);
		transport.connect(host, username, password);
		msg.saveChanges();
		transport.sendMessage(msg, msg.getAllRecipients());

	}

	// A simple, single-part text/plain e-mail.
	public void setTextContent(String plainText) throws MessagingException,
			SessionCreationException {

		SMTPMessage msg = setAddress();
		msg.setText(plainText);
		msg.setContent(plainText, "text/plain");
		msg.saveChanges();
		sendMail(msg);
		logger.info("email has been sent.");
	}

	// A simple multipart/mixed e-mail. Both body parts are text/plain.
	public void setMultipartContent(String plainText1, String plainText2)
			throws MessagingException, SessionCreationException {
		// Create and fill first part
		SMTPMessage msg = setAddress();
		MimeBodyPart p1 = new MimeBodyPart();
		p1.setContent(plainText1, "text/html");

		// Create and fill second part
		MimeBodyPart p2 = new MimeBodyPart();
		// Here is how to set a charset on textual content
		p2.setText(plainText2);

		// Create the Multipart. Add BodyParts to it.
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(p1);
		mp.addBodyPart(p2);

		// Set Multipart as the message's content
		msg.setContent(mp);
		msg.saveChanges();
		sendMail(msg);
		logger.info(" html content email has been sent.");
	}

	// Set a file as an attachment. Uses JAF FileDataSource.
	public void setFileAsAttachment(String text, String[] attachments)
			throws MessagingException, FileNotFoundException,
			SessionCreationException {

		SMTPMessage msg = setAddress();
		// Create and fill first part
		MimeBodyPart p1 = new MimeBodyPart();
		p1.setText(text);
		logger.info("Create and fill first part");
		// Create the Multipart. Add BodyParts to it.
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(p1);
		addAtachments(attachments, mp);
		logger.info(" Create the Multipart. Add BodyParts to it");
		// Set Multipart as the message's content
		msg.setContent(mp);
		logger.info(" Set Multipart as the message's content");
		msg.saveChanges();
		sendMail(msg);
		for (String file : attachments) {
			logger.info("attachments has sent. The attachments is " + file);
		}

	}

	public void addAtachments(String[] attachments, Multipart multipart)
			throws MessagingException, AddressException {
		for (int i = 0; i <= attachments.length - 1; i++) {
			String filename = attachments[i];
			MimeBodyPart attachmentBodyPart = new MimeBodyPart();

			DataSource source = new FileDataSource(filename);
			attachmentBodyPart.setDataHandler(new DataHandler(source));

			// assume that the filename you want to send is the same as the

			attachmentBodyPart.setFileName(filename);

			// add the attachment
			multipart.addBodyPart(attachmentBodyPart);

		}
	}

	// Set a single part html content.
	// Sending data of any type is similar.
	public void setHTMLContent(String html) throws MessagingException,
			SessionCreationException {
		SMTPMessage msg = setAddress();
		// HTMLDataSource is a static nested class
		msg.setDataHandler(new DataHandler(new HTMLDataSource(html)));
		msg.saveChanges();
		sendMail(msg);
	}

	public void setHTMLContent(EmailBean emailBean, String html)
			throws MessagingException, SessionCreationException {
		SMTPMessage msg = setAddress(emailBean);
		// HTMLDataSource is a static nested class
		msg.setDataHandler(new DataHandler(new HTMLDataSource(html)));
		msg.saveChanges();
		sendMail(msg);
	}

	public class HTMLDataSource implements DataSource {
		private String html;

		public HTMLDataSource(String htmlString) {
			html = htmlString;
		}

		public InputStream getInputStream() throws IOException {
			if (html == null)
				throw new IOException("Null HTML");
			return new ByteArrayInputStream(html.getBytes());
		}

		public OutputStream getOutputStream() throws IOException {
			throw new IOException("This DataHandler cannot write HTML");
		}

		public String getContentType() {
			return "text/html";
		}

		public String getName() {
			return "JAF text/html dataSource to send e-mail only";
		}
	}

	/**
	 * Method to send a mail for add user
	 * @param bean
	 * @param content
	 * @param userImage
	 * @throws MessagingException
	 * @throws SessionCreationException
	 * @throws MalformedURLException 
	 */
	public void sendMail(EmailBean bean, String html, File userImage,
			File logo) throws MessagingException, SessionCreationException, MalformedURLException {
		
		List<ImageResource> imageResources = new ArrayList<ImageResource>();
		imageResources.add(new ImageResource(userImage.getPath(),userImage.getName(),"<image>",false));
		imageResources.add(new ImageResource(logo.getPath(),logo.getName(),"<logo>",false));
		
        SMTPMessage msg = setAddress(bean);

		MimeMultipart multipart = new MimeMultipart("related");
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(html, "text/html");
		multipart.addBodyPart(htmlPart);
	
		// images
		for (ImageResource imageResource : imageResources) {
			String path = imageResource.getPath();
			String fileName = imageResource.getName();
			String cid = imageResource.getCid();
			boolean isURL = imageResource.isURL();
			addImageWithCid(multipart, path, fileName, cid, isURL);
		}
		
		msg.setContent(multipart);

		Transport transport = session.getTransport(bean.getMailTransportProtocol());
		transport.connect(bean.getMailHost(), bean.getUsername(),bean.getPassword());
		msg.saveChanges();
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
	}

	/**
	 * Method to send attachement with email
	 * @throws MessagingException
	 * @throws MalformedURLException
	 */
	public void addAttachment(Multipart multipart, 
			Resource attachment) throws MessagingException, MalformedURLException {
		String attachmentPath = attachment.getPath();
		String attachmentName = attachment.getName();
		boolean isURL = attachment.isURL();
		if(isURL) {
			BodyPart attachmentBodyPart = new MimeBodyPart();
			URL attachmentURL = new URL(attachmentPath);
			URLDataSource source = new URLDataSource(attachmentURL);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			attachmentBodyPart.setFileName(attachmentName);
			multipart.addBodyPart(attachmentBodyPart);			
		} else {
			BodyPart attachmentBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachmentPath);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			attachmentBodyPart.setFileName(attachmentName);
			multipart.addBodyPart(attachmentBodyPart);
		}
	}
	
	
	
	/**
	 * Method to send a images without attachment
	 * @throws MessagingException
	 * @throws MalformedURLException
	 */
	public void addImageWithCid(
			Multipart multipart, 
			String imageFilePath,
			String imageFileName,
			String imageFileCid, 
			boolean isURL) throws MessagingException, MalformedURLException {		
		if(isURL) {
			BodyPart imgPart = new MimeBodyPart();
			URL imageFileURL = new URL(imageFilePath);
			URLDataSource ds = new URLDataSource(imageFileURL);
			imgPart.setDataHandler(new DataHandler(ds));
			imgPart.setHeader("Content-ID", imageFileCid);
			imgPart.setFileName(imageFileName);
			multipart.addBodyPart(imgPart);
		} else {
			BodyPart imgPart = new MimeBodyPart();
			DataSource ds = new FileDataSource(imageFilePath);
			imgPart.setDataHandler(new DataHandler(ds));
			imgPart.setHeader("Content-ID", imageFileCid);
			imgPart.setFileName(imageFileName);
			multipart.addBodyPart(imgPart);
		}
	}
	
	
	/**
	 * Method to send a mail for file and folder operation
	 * @param bean
	 * @param content
	 * @param userImg
	 * @param operation
	 * @param fileType
	 * @throws MessagingException
	 * @throws SessionCreationException
	 * @throws MalformedURLException 
	 */
	public void sendMail(EmailBean bean, String html, File userImg,
			File logo, File file, File icon) throws SessionCreationException,
			MessagingException, MalformedURLException {
		
		List<ImageResource> imageResources = new ArrayList<ImageResource>();
		imageResources.add(new ImageResource(userImg.getPath(),userImg.getName(),"<image>",false));
		imageResources.add(new ImageResource(logo.getPath(),logo.getName(),"<logo>",false));
		imageResources.add(new ImageResource(file.getPath(),file.getName(),"<file>",false));
		imageResources.add(new ImageResource(icon.getPath(),icon.getName(),"<icon>",false));
		
		//for attachment
		/*List<Resource> attachments = new ArrayList<Resource>();
		attachments.add(new Resource("file1.txt", "attached-file1.txt", false));*/
		
        SMTPMessage msg = setAddress(bean);

		MimeMultipart multipart = new MimeMultipart("related");
		
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(html, "text/html");
		multipart.addBodyPart(htmlPart);
	
		// images
		for (ImageResource imageResource : imageResources) {
			String path = imageResource.getPath();
			String fileName = imageResource.getName();
			String cid = imageResource.getCid();
			boolean isURL = imageResource.isURL();
			addImageWithCid(multipart, path, fileName, cid, isURL);
		}
		
		// attachments
			/*	for (Resource attachment : attachments) {
					addAttachment(multipart, attachment);
				}*/
		
		msg.setContent(multipart);

		Transport transport = session.getTransport(bean.getMailTransportProtocol());
		transport.connect(bean.getMailHost(), bean.getUsername(),bean.getPassword());
		msg.saveChanges();
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
	}

	// Sending message.
	public Session getSession(EmailBean bean) throws SessionCreationException {
		logger.debug("Sessoin created.");
		session = createUserMailSession(bean);
		return session;
	}

	private Session createUserMailSession(final EmailBean bean)
			throws SessionCreationException {
		Properties props = defaultMailConfig(bean);
		return Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(bean.getUsername(), bean
						.getPassword());
			}
		});
	}

	public Properties defaultMailConfig(EmailBean bean)
			throws SessionCreationException {

		Properties property = new Properties();
		property.put("mail.transport.protocol", bean.getMailTransportProtocol());
		property.put("mail.host", bean.getMailHost());
		property.put("mail.auth", "true");
		return property;
	}
}
