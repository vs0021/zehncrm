/*
 Name                    Date               version                     Description
 soagrawa               13/March/2014        1.0                        This class contain logic for convert html to text.                   
 */
package com.zehntech.matterhorn.service.email.impl;

import java.io.Reader;
import java.io.StringReader;

import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;

import com.zehntech.matterhorn.logging.ApplicationLogger;

public class HtmlToTextConverter {
	ApplicationLogger logger = ApplicationLogger
			.getLogger(HtmlToTextConverter.class);

	// This method is used for return simple string.
	public final String html2text(String html) {
		EditorKit kit = new HTMLEditorKit();
		Document doc = kit.createDefaultDocument();
		doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
		try {
			Reader reader = new StringReader(html);
			kit.read(reader, doc, 0);
			logger.info("HTML contentconverted in to text ");
			return doc.getText(0, doc.getLength());

		} catch (Exception e) {
			return "";
		}
	}

}
