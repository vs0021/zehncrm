package com.zehntech.matterhorn.service.email.impl;

public class SessionCreationException extends Exception {

	private static final long serialVersionUID = 1L;

	public SessionCreationException() {
		super(
				"Error in creating session, one or more of the required properties are missing");
	}

	public SessionCreationException(String s) {
		super(s);
	}
}
