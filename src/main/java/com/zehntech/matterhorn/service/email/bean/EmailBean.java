/*
 Name                    Date               version                     Description
 soagrawa              13/March/2014        1.0                        This is class for initializing required fields of email.                   
 */

package com.zehntech.matterhorn.service.email.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import org.apache.commons.lang3.StringUtils;

public class EmailBean {
	private String to;

	private String from;

	private String fileId;

	private String cc;

	private String bcc;

	private String version;

	private String subject;

	private String username;

	private String password;

	private String message;

	private String fileName;

	private String mailTransportProtocol;

	private String mailStoreProtocol;

	private String mailHost;

	private int maxDownload;

	private String attachmentLinks;

	private String ValidDate;

	private String url;

	private String radio;

	private String readReciept;

	private String folderId;

	private List<String> fileNames;

	private String fromName;

	private String replyTo;

	private String inReplyTo;

	private String mimeType;

	private String encoding;

	private String messageId;

	private Map<String, String> headers;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getMaxDownload() {
		return maxDownload;
	}

	public void setMaxDownload(int maxDownload) {
		this.maxDownload = maxDownload;
	}

	public String getAttachmentLinks() {
		return attachmentLinks;
	}

	public void setAttachmentLinks(String attachmentLinks) {
		this.attachmentLinks = attachmentLinks;
	}

	public String getValidDate() {
		return ValidDate;
	}

	public void setValidDate(String validDate) {
		ValidDate = validDate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}

	public String getReadReciept() {
		return readReciept;
	}

	public void setReadReciept(String readReciept) {
		this.readReciept = readReciept;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public List<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public ApplicationLogger getLogger() {
		return logger;
	}

	public void setLogger(ApplicationLogger logger) {
		this.logger = logger;
	}

	ApplicationLogger logger = ApplicationLogger.getLogger(EmailBean.class);

	public EmailBean(String to, String cc, String bcc) {
		logger.debug("emailBean has been initialized");
		if (StringUtils.isBlank(to) && StringUtils.isBlank(cc)
				&& StringUtils.isBlank(bcc)) {

			throw new IllegalArgumentException(
					"'To' or 'cc' or 'bcc' is a required field");
		}

		this.cc = cc;
		this.bcc = bcc;
		this.to = to;
		this.mimeType = "text/plain";
		this.encoding = "UTF-8";
		this.headers = new HashMap<String, String>();
		loadDefaultHeaders();
	}

	public EmailBean() {
		logger.debug("emailBean has been initialized");
	}

	// By default the message has the "Precedence" header set to "bulk".
	protected void loadDefaultHeaders() {

		headers.put("Precedence", "bulk");
		headers.put("Auto-Submitted", "auto-generated");
	}

	public String getTo() {
		return to;
	}

	public EmailBean(String to, String from, String fromName, String cc,
			String bcc, String replyTo, String inReplyTo, String messageId,
			String subject) {
		this(to, cc, bcc);
		if (StringUtils.isBlank(from)) {

			throw new IllegalArgumentException("'From' is a required field");
		}

		this.from = from;
		this.fromName = fromName;
		this.replyTo = replyTo;
		this.inReplyTo = inReplyTo;
		this.messageId = messageId;
		this.subject = subject;
		logger.debug("emailBean has initialized");

	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public void setInReplyTo(String inReplyTo) {
		this.inReplyTo = inReplyTo;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	@Override
	public String toString() {
		return "EmailBean [to=" + to + ", from=" + from + ", fileId=" + fileId
				+ ", cc=" + cc + ", bcc=" + bcc + ", version=" + version
				+ ", subject=" + subject + ", password=" + password
				+ ", message=" + message + ", fileName=" + fileName
				+ ", maxDownload=" + maxDownload + ", attachmentLinks="
				+ attachmentLinks + ", ValidDate=" + ValidDate + ", url=" + url
				+ ", radio=" + radio + ", readReciept=" + readReciept
				+ ", folderId=" + folderId + ", fileNames=" + fileNames
				+ ", fromName=" + fromName + ", replyTo=" + replyTo
				+ ", inReplyTo=" + inReplyTo + ", mimeType=" + mimeType
				+ ", encoding=" + encoding + ", messageId=" + messageId
				+ ", headers=" + headers + ", logger=" + logger + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ValidDate == null) ? 0 : ValidDate.hashCode());
		result = prime * result
				+ ((attachmentLinks == null) ? 0 : attachmentLinks.hashCode());
		result = prime * result + ((bcc == null) ? 0 : bcc.hashCode());
		result = prime * result + ((cc == null) ? 0 : cc.hashCode());
		result = prime * result
				+ ((encoding == null) ? 0 : encoding.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result
				+ ((fileNames == null) ? 0 : fileNames.hashCode());
		result = prime * result
				+ ((folderId == null) ? 0 : folderId.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result
				+ ((fromName == null) ? 0 : fromName.hashCode());
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		result = prime * result
				+ ((inReplyTo == null) ? 0 : inReplyTo.hashCode());
		result = prime * result + ((logger == null) ? 0 : logger.hashCode());
		result = prime * result + maxDownload;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result
				+ ((messageId == null) ? 0 : messageId.hashCode());
		result = prime * result
				+ ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((radio == null) ? 0 : radio.hashCode());
		result = prime * result
				+ ((readReciept == null) ? 0 : readReciept.hashCode());
		result = prime * result + ((replyTo == null) ? 0 : replyTo.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailBean other = (EmailBean) obj;
		if (ValidDate == null) {
			if (other.ValidDate != null)
				return false;
		}
		else if (!ValidDate.equals(other.ValidDate))
			return false;
		if (attachmentLinks == null) {
			if (other.attachmentLinks != null)
				return false;
		}
		else if (!attachmentLinks.equals(other.attachmentLinks))
			return false;
		if (bcc == null) {
			if (other.bcc != null)
				return false;
		}
		else if (!bcc.equals(other.bcc))
			return false;
		if (cc == null) {
			if (other.cc != null)
				return false;
		}
		else if (!cc.equals(other.cc))
			return false;
		if (encoding == null) {
			if (other.encoding != null)
				return false;
		}
		else if (!encoding.equals(other.encoding))
			return false;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		}
		else if (!fileId.equals(other.fileId))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		}
		else if (!fileName.equals(other.fileName))
			return false;
		if (fileNames == null) {
			if (other.fileNames != null)
				return false;
		}
		else if (!fileNames.equals(other.fileNames))
			return false;
		if (folderId == null) {
			if (other.folderId != null)
				return false;
		}
		else if (!folderId.equals(other.folderId))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		}
		else if (!from.equals(other.from))
			return false;
		if (fromName == null) {
			if (other.fromName != null)
				return false;
		}
		else if (!fromName.equals(other.fromName))
			return false;
		if (headers == null) {
			if (other.headers != null)
				return false;
		}
		else if (!headers.equals(other.headers))
			return false;
		if (inReplyTo == null) {
			if (other.inReplyTo != null)
				return false;
		}
		else if (!inReplyTo.equals(other.inReplyTo))
			return false;
		if (logger == null) {
			if (other.logger != null)
				return false;
		}
		else if (!logger.equals(other.logger))
			return false;
		if (maxDownload != other.maxDownload)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		}
		else if (!message.equals(other.message))
			return false;
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		}
		else if (!messageId.equals(other.messageId))
			return false;
		if (mimeType == null) {
			if (other.mimeType != null)
				return false;
		}
		else if (!mimeType.equals(other.mimeType))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		}
		else if (!password.equals(other.password))
			return false;
		if (radio == null) {
			if (other.radio != null)
				return false;
		}
		else if (!radio.equals(other.radio))
			return false;
		if (readReciept == null) {
			if (other.readReciept != null)
				return false;
		}
		else if (!readReciept.equals(other.readReciept))
			return false;
		if (replyTo == null) {
			if (other.replyTo != null)
				return false;
		}
		else if (!replyTo.equals(other.replyTo))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		}
		else if (!subject.equals(other.subject))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		}
		else if (!to.equals(other.to))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		}
		else if (!url.equals(other.url))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		}
		else if (!version.equals(other.version))
			return false;
		return true;
	}

	public String getFrom() {
		return from;
	}

	public String getFromName() {
		return fromName;
	}

	public String getCc() {
		return cc;
	}

	public String getBcc() {
		return bcc;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public String getInReplyTo() {
		return inReplyTo;
	}

	public String getSubject() {
		return subject;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getEncoding() {
		return encoding;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void addHeader(String headerName, String headerValue) {
		headers.put(headerName, headerValue);
	}

	public String removeHeader(String headerName) {
		if (headers.containsKey(headerName)) {
			return (String) headers.remove(headerName);
		}
		else {
			return null;
		}
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getMailTransportProtocol() {
		return mailTransportProtocol;
	}

	public void setMailTransportProtocol(String mailTransportProtocol) {
		this.mailTransportProtocol = mailTransportProtocol;
	}

	public String getMailStoreProtocol() {
		return mailStoreProtocol;
	}

	public void setMailStoreProtocol(String mailStoreProtocol) {
		this.mailStoreProtocol = mailStoreProtocol;
	}

	public String getMailHost() {
		return mailHost;
	}

	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}

}
