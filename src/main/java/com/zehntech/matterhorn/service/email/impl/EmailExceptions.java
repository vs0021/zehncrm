/*
 Name                    Date               version                     Description
 soagrawa              13/March/2014        1.0                        This class is for user define exception.                   
 */
package com.zehntech.matterhorn.service.email.impl;

public class EmailExceptions extends Exception {

	private static final long serialVersionUID = 1L;

	public EmailExceptions() {
	}

	public EmailExceptions(String s) {
		super(s);
	}

	public EmailExceptions(String s, Throwable throwable) {
		super(s, throwable);
	}
}
