/*
 Name                    Date               version                     Description
 soagrawa               13/March/2014        1.0                        This class is providing mail session, taking authentication from property file.                   
 */
package com.zehntech.matterhorn.service.email.impl;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import com.sun.mail.util.MailSSLSocketFactory;
import com.zehntech.matterhorn.logging.ApplicationLogger;

public class EmailUtil {

	private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";

	private static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";

	private static final String MAIL_SMTP_SSL_SOCKETFACTORY = "mail.smtp.ssl.socketFactory";

	private String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";

	private String MAIL_STORE_PROTOCOL = "mail.store.protocol";

	private String MAIL_HOST = "mail.host";

	private Session session;

	ApplicationLogger logger = ApplicationLogger.getLogger(EmailUtil.class);

	private String mailTransportProtocol;

	private String mailStoreProtocol;

	private String mailHost;

	private Properties props = null;

	public EmailUtil(Properties props) {
		logger.debug("Load username, password, host name and port from properpty file.");
		mailTransportProtocol = props.getProperty(MAIL_TRANSPORT_PROTOCOL);
		logger.debug("Mail mailTransportProtocol " + mailTransportProtocol);
		mailStoreProtocol = props.getProperty(MAIL_STORE_PROTOCOL);
		logger.debug("Mail mailStoreProtocol " + mailStoreProtocol);

		mailHost = props.getProperty(MAIL_HOST);
		logger.debug("Mail mailHost " + mailHost);
		this.props = props;

	}

	// Sending message.
	public Session getSession(String username, String password)
			throws SessionCreationException {
		logger.debug("Sessoin created.");
		session = createUserMailSession(username, password);
		logger.debug("Found store Protocol "
				+ session.getProperty("mail.store.protocol"));
		logger.debug("Found transport Protocol "
				+ session.getProperty("mail.transport.protocol"));
		logger.debug(" Create user mail session.");
		return session;
	}

	private Session createUserMailSession(final String username,
			final String password) throws SessionCreationException {
		Properties props = defaultMailConfig();
		return Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

	}

	public Properties defaultMailConfig() throws SessionCreationException {

		Properties property = new Properties();
		// property.put(MAIL_SERVER_PROTOCOL,
		// props.getProperty(MAIL_SERVER_PROTOCOL));
		// property.put(MAIL_SMTP_PROTOCOL,
		// props.getProperty(MAIL_SMTP_PROTOCOL));
		// property.put(MAIL_SMTP_AUTH, props.getProperty(MAIL_SMTP_AUTH));
		// property.put(MAIL_SMTP_STARTTLS_ENABLE,
		// props.getProperty(MAIL_SMTP_STARTTLS_ENABLE));
		// property.put(MAIL_SMTP_HOST, props.getProperty(MAIL_SMTP_HOST));
		// property.put(MAIL_SMTP_PORT, port);
		// // property.put(MAIL_TIMEOUT, props.getProperty(MAIL_TIMEOUT));
		// property.put(MAIL_SMTP_SSL_SOCKETFACTORY, mailSslSocketFactory());
		if (mailStoreProtocol == null && mailTransportProtocol == null) {
			logger.debug("they are null");
			throw new SessionCreationException();
		}
		else {
			logger.debug("not null");
		}

		if (props.getProperty("mail.connectiontimeout") != null) {
			if (props.getProperty("mail.store.protocol") != null) {
				logger.debug("Set timeout for imap " + property);
				property.put("mail.imap.user", props.getProperty("mail.user"));
				property.put("mail.store.protocol",
						props.getProperty("mail.store.protocol"));
				property.put("mail." + props.getProperty("mail.store.protocol")
						+ ".host", props.getProperty("mail.host"));

				property.put("mail." + props.getProperty("mail.store.protocol")
						+ ".connectiontimeout",
						props.getProperty("mail.connectiontimeout"));

				property.put("mail." + props.getProperty("mail.store.protocol")
						+ ".ssl.enable", props.getProperty("mail.tls.enabled"));

				property.put("mail." + props.getProperty("mail.store.protocol")
						+ ".starttls.enable",
						props.getProperty("mail.tls.enabled"));

				property.put("mail." + props.getProperty("mail.store.protocol")
						+ ".port", props.getProperty("mail.port"));

			}

		}
		return property;
	}

	private MailSSLSocketFactory mailSslSocketFactory() {
		MailSSLSocketFactory sf = null;
		try {
			sf = new MailSSLSocketFactory();
		}
		catch (GeneralSecurityException e) {
			logger.debug(e.getMessage());
		}
		sf.setTrustAllHosts(true); // If you'd like to trust only some
									// SSL-certificates, create an array
		return sf;
	}

	public List<Message> filterBySubject(List<Message> messages, String regex,
			boolean isCaseSensitive) {
		logger.debug("Filtering by subject....");
		logger.debug("Foud regular expression to match " + regex);
		logger.debug("isCaseSensitive " + isCaseSensitive);
		Pattern pattern = null;
		List<Message> matchedMsgs = new ArrayList<Message>();
		if (isCaseSensitive) {
			pattern = Pattern.compile(regex);
		}
		else {
			pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		}
		for (Message msg : messages) {
			try {
				if (msg.getSubject() != null || (!msg.getSubject().equals(""))) {
					logger.debug("Found subject as " + msg.getSubject());
					Matcher matcher = pattern.matcher(msg.getSubject());

					if (matcher.matches()) {
						logger.debug("Match found");
						matchedMsgs.add(msg);
					}
				}
			}
			catch (MessagingException e) {
				return null;
			}
		}
		return matchedMsgs;
	}

	public boolean checkFilename(String filename){
		String regex = "[\\s\\S]*[:/<>\\\\\"?|*]+[\\s\\S]*";
		if(filename.matches(regex) || filename.indexOf(".")==0){
			return false;
		}
		return true;
	}
	
	
	public boolean doesContentMatch(String content, String regex,
			boolean isCaseSensitive) {
		if (content == null || regex == null) {
			return false;
		}
		String javaRegex = convertToJavaRegex(regex, isCaseSensitive);
		if (javaRegex == null) {
			logger.debug("Java regex found to be null");
			return false;
		}
		else {
			logger.debug("Java regex is not null " + javaRegex);
		}
		boolean negation = javaRegex.startsWith("!");
		if (negation) {
			logger.debug("Regex is negation");
			javaRegex = javaRegex.substring(1);
		}

		Pattern pattern = null;

		if (isCaseSensitive) {
			pattern = Pattern.compile(javaRegex);
		}
		else {
			pattern = Pattern.compile(javaRegex, Pattern.CASE_INSENSITIVE);
		}

		Matcher matcher = pattern.matcher(content);
		if (negation) {
			return !(matcher.matches());
		}
		
		if(content.contains(".")){
			if(content.substring(0, content.lastIndexOf(".")).equals(regex)){
				return true;
			}
		}
		
		return matcher.matches();
	}

	public String convertToJavaRegex(String flumsRegex, boolean isCaseSensitive) {
		logger.debug("Converting to java regular expression");
		logger.debug("Flums regex found here " + flumsRegex);
		if (flumsRegex == null) {
			logger.debug("Flums regular expression found to be null");
			return null;
		}
		if (flumsRegex.equals("*")) {
			logger.debug("Flums regex matched here ");
			return "[\\s\\S]*";
		}
		String flmRegex = flumsRegex.trim();
		if (flmRegex.startsWith("!")) {
			logger.debug("Flums regex starts with !");
			flmRegex = flmRegex.substring(1);
		}
		else {
			logger.debug("Flums regex does not start with !");
		}

		String str = matchWithContentContainsRegex(flmRegex, isCaseSensitive);
		if (str != null) {
			if (flumsRegex.startsWith("!")) {
				logger.debug("FlumsRegex match with content does not contain "
						+ str + " regex");
				return "![\\s\\S]*" + str + "[\\s\\S]*";
			}
			else {
				logger.debug("FlumsRegex match with content contains " + str
						+ " regex");
				return "[\\s\\S]*" + str + "[\\s\\S]*";
			}
		}
		str = matchWithContentStartsWithRegex(flmRegex, isCaseSensitive);
		if (str != null) {
			if (flumsRegex.startsWith("!")) {
				logger.debug("FlumsRegex match with content does not start with "
						+ str + " regex");
				return "!\\s*" + str + "[\\s\\S]*";
			}
			else {
				logger.debug("FlumsRegex match with content starts with " + str
						+ " regex");
				return "\\s*" + str + "[\\s\\S]*";
			}
		}
		str = matchWithContentEndsWithRegex(flmRegex, isCaseSensitive);
		if (str != null) {
			if (flumsRegex.startsWith("!")) {
				logger.debug("FlumsRegex match with content does not end with "
						+ str + " regex");
				return "![\\s\\S]*" + str + "\\s*";
			}
			else {
				logger.debug("FlumsRegex match with content ends with " + str
						+ " regex");
				return "[\\s\\S]*" + str + "\\s*";
			}
		}
		str = matchWithContentExactMatchRegex(flmRegex, isCaseSensitive);
		if (str != null) {
			if (flumsRegex.startsWith("!")) {
				logger.debug("FlumsRegex match with content does not exactly match with "
						+ str + " regex");
				return "!" + str;
			}
			else {
				logger.debug("FlumsRegex match with content exactly matches with "
						+ str + " regex");
				return str;
			}
		}

		 return flumsRegex+"*";
		//return null;
	}

	public String matchWithContentContainsRegex(String flumsRegex,
			boolean isCaseSensitive) {

		if (!flumsRegex.startsWith("*") || !flumsRegex.endsWith("*")
				|| flumsRegex.length() <= 2) {
			return null;
		}
		else if (flumsRegex.endsWith("*")) {
			if (flumsRegex.charAt(flumsRegex.length() - 2) == '\\') {
				return null;
			}

		}
		return flumsRegex.substring(1, flumsRegex.length() - 1);

	}

	public String matchWithContentStartsWithRegex(String flumsRegex,
			boolean isCaseSensitive) {

		if (!flumsRegex.endsWith("*") || flumsRegex.startsWith("*")
				|| flumsRegex.length() <= 1
				|| flumsRegex.charAt(flumsRegex.length() - 2) == '\\') {
			return null;
		}

		return flumsRegex.substring(0, flumsRegex.length() - 1);
	}

	public String matchWithContentEndsWithRegex(String flumsRegex,
			boolean isCaseSensitive) {
		if (!flumsRegex.startsWith("*") || flumsRegex.length() <= 1) {
			return null;
		}
		if (flumsRegex.endsWith("*")) {
			if (flumsRegex.charAt(flumsRegex.length() - 2) != '\\') {
				return null;
			}

		}

		return flumsRegex.substring(1);
	}

	public String matchWithContentExactMatchRegex(String flumsRegex,
			boolean isCaseSensitive) {
		if (!flumsRegex.contains("*") || flumsRegex.indexOf("*") == 0) {
			return null;
		}
		if (flumsRegex.length() > 1) {
			char[] c = flumsRegex.toCharArray();
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '*' && c[i - 1] != '\\') {
					return null;
				}
			}
		}

		return flumsRegex;
	}
}
