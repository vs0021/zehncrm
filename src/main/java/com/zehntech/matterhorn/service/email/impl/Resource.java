package com.zehntech.matterhorn.service.email.impl;

public class Resource {
	private String path;
	private String name;
	private boolean isURL;
	public Resource() {
		
	}
	public Resource(String path, boolean isURL) {
		this(path, path, isURL);
	}
	public Resource(String path, String name, boolean isURL) {
		this.path = path;
		this.name = name;
		this.isURL = isURL;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isURL() {
		return isURL;
	}
	public void setURL(boolean isURL) {
		this.isURL = isURL;
	}
}