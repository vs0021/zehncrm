package com.zehntech.matterhorn.service.email.impl;

public class ImageResource extends Resource {
	private String cid;
	public ImageResource() {
		super();
	}
	public ImageResource(String path, String name, String cid, boolean isURL) {
		super(path, name, isURL);
		this.cid = cid;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
}
