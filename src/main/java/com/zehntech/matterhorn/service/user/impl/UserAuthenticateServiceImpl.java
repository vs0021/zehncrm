package com.zehntech.matterhorn.service.user.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.zehntech.matterhorn.dao.user.UserDao;
import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.domain.user.AuthUsersData;
import com.zehntech.matterhorn.domain.user.CustomUserDetailData;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import com.zehntech.matterhorn.service.user.UserAuthenticateService;

@Service("userAuthenticateService")
public class UserAuthenticateServiceImpl implements UserAuthenticateService {

	ApplicationLogger logger = ApplicationLogger
			.getLogger(UserAuthenticateServiceImpl.class);

	@Autowired
	private UserDao userDao;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		AuthUsersData authUsersData;
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		CompanyTenantData companyTenantData = null;
		List<Object> findAuthUsersAndUserDetail = null;
		Date date = null;
		Calendar calendar = null;
		int expirationValue = 1;
		boolean licenceExpired = false;

		findAuthUsersAndUserDetail = userDao
				.findAuthUsersAndUserDetail(username);
		authUsersData = (AuthUsersData) findAuthUsersAndUserDetail.get(0);
		companyTenantData = (CompanyTenantData) findAuthUsersAndUserDetail
				.get(1);
		if (authUsersData == null) {
			throw new UsernameNotFoundException("User Not Found!!!");
		}

		System.out.println("authentication started for the user::" + username
				+ authUsersData.getCompanyTenantData());
		String password = authUsersData.getPassword();
		boolean enabled = authUsersData.getEnabled();
		boolean accountNonExpired = authUsersData.getEnabled();
		boolean credentialsNonExpired = authUsersData.getEnabled();
		boolean accountNonLocked = authUsersData.getEnabled();
		// userDetailData = getUserDetail(authUsersData);
		System.out.println("authentication started for the user::" + username
				+ authUsersData);
		if (authUsersData != null) {
			try {

				// companyTenantData = authUsersData.getCompanyTenantData();
				calendar = Calendar.getInstance();
				date = new Date();
				Timestamp currentTimestamp = new Timestamp(date.getTime());
				logger.debug("currentTimestamp date::" + currentTimestamp);
				logger.debug("expiration date::"
						+ companyTenantData.getExpiryDate());
				/*
				 * expirationValue =
				 * companyTenantData.getExpiryDate().compareTo( new
				 * Timestamp(date.getTime()));
				 */
				logger.debug("Expiration value::" + expirationValue);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		// AuthAuthorityData authAuthorityData = new AuthAuthorityData();
		// authAuthorityData.setAuthority("CLOUDCONTROLLER");

		try {

			logger.debug("companyTenant status:" + companyTenantData.isStatus());
			if (expirationValue == -1 || !companyTenantData.isStatus()) {
				logger.info("licenceExpired:" + true);
				licenceExpired = true;

			}

			logger.debug("loadUserByUsername authGroupData Group::");
		}
		catch (Exception e) {

		}

		authorities.add(new GrantedAuthority() {

			public String getAuthority() {
				// TODO Auto-generated method stub
				return "USER";
			}
		});

		CustomUserDetailData springUser = new CustomUserDetailData(username,
				password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		/*
		 * if (userDetailData.getAuthUsersData().getCompanyTenantData() != null)
		 * { springUser.setMap(getUserPermissions(authUsersData)); }
		 */
		authorities = null;

		return springUser;

	}
}
