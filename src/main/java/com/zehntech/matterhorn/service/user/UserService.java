package com.zehntech.matterhorn.service.user;

import com.zehntech.matterhorn.bean.forms.CompanyAdminDetailForm;
import com.zehntech.matterhorn.bean.forms.UserDetailForm;
import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.utility.exception.ObjectNotFound;
import com.zehntech.matterhorn.utility.exception.OperationNotSupportedException;

public interface UserService {
	void addCompanyTenant(CompanyAdminDetailForm companyAdminDetailForm)
			throws OperationNotSupportedException, ObjectNotFound, Exception;

	public void addUsersData(UserDetailForm userDetailForm,
			CompanyTenantData companyTenantData)
			throws OperationNotSupportedException, ObjectNotFound, Exception;
}
