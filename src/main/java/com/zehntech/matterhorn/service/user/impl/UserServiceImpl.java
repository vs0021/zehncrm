package com.zehntech.matterhorn.service.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zehntech.matterhorn.bean.forms.CompanyAdminDetailForm;
import com.zehntech.matterhorn.bean.forms.UserDetailForm;
import com.zehntech.matterhorn.dao.user.UserDao;
import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.domain.user.AuthUsersData;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import com.zehntech.matterhorn.service.emailService.EmailService;
import com.zehntech.matterhorn.service.user.UserService;
import com.zehntech.matterhorn.utility.Hashing;
import com.zehntech.matterhorn.utility.UserUtil;
import com.zehntech.matterhorn.utility.exception.ObjectNotFound;
import com.zehntech.matterhorn.utility.exception.OperationNotSupportedException;

@Service("UserService")
public class UserServiceImpl implements UserService {

	ApplicationLogger logger = ApplicationLogger
			.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserUtil userUtil;

	@Autowired
	private EmailService emailService;

	@Autowired
	private Hashing hashing;

	public void addCompanyTenant(CompanyAdminDetailForm companyAdminDetailForm)
			throws OperationNotSupportedException, ObjectNotFound, Exception {
		CompanyTenantData companyTenantData = null;
		companyTenantData = userUtil
				.populateCompanyTenantData(companyAdminDetailForm);
		companyTenantData = userDao.addCompanyTenant(companyTenantData);

		UserDetailForm userDetailForm = new UserDetailForm();
		userDetailForm.setLogin(companyAdminDetailForm.getLogin());
		userDetailForm.setUserName(companyAdminDetailForm.getUserName());
		userDetailForm.setPassword(hashing
				.SHA256Hashing16Bit(companyAdminDetailForm.getPassword()));
		userDetailForm.setNotify(true);

		userDetailForm.setPasswordGenerateType(1);
		userDetailForm.setPhoneNumber(companyAdminDetailForm.getPhoneNumber());

		addUsersData(userDetailForm, companyTenantData);

		emailService.sendAddUserMail("Admin", userDetailForm.getDisplayName(),
				userDetailForm.getLogin(), userDetailForm.getUserId(),
				"Hii i am url");
	
	}

	public void addUsersData(UserDetailForm userDetailForm,
			CompanyTenantData companyTenantData)
			throws OperationNotSupportedException, ObjectNotFound, Exception {
		AuthUsersData authUsersData = null;

		authUsersData = userUtil.populateAuthUsersData(userDetailForm);
		authUsersData.setCompanyTenantData(companyTenantData);
		userDao.addUsersData(authUsersData);
	}

}
