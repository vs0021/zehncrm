package com.zehntech.matterhorn.service.user;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserAuthenticateService extends UserDetailsService {

}
