package com.zehntech.matterhorn.service.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

	public ClientDetails loadClientByClientId(String clientId)
			throws OAuth2Exception {

		if (clientId.equals("client1")) {

			List<String> authorizedGrantTypes = new ArrayList<String>();
			authorizedGrantTypes.add("password");
			authorizedGrantTypes.add("refresh_token");
			authorizedGrantTypes.add("client_credentials");
			authorizedGrantTypes.add("implicit");
			authorizedGrantTypes.add("authorization_code");
			BaseClientDetails clientDetails = new BaseClientDetails();
			clientDetails.setClientId("client1");
			clientDetails.setClientSecret("client1");
			clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
			List<String> ScopeList = new ArrayList<String>();
			ScopeList.add("read");
			ScopeList.add("write");
			ScopeList.add("trust");

			clientDetails.setScope(ScopeList);
			return clientDetails;

		}
		else if (clientId.equals("client2")) {

			List<String> authorizedGrantTypes = new ArrayList<String>();
			authorizedGrantTypes.add("password");
			authorizedGrantTypes.add("refresh_token");
			authorizedGrantTypes.add("client_credentials");
			authorizedGrantTypes.add("implicit");
			authorizedGrantTypes.add("authorization_code");
			BaseClientDetails clientDetails = new BaseClientDetails();
			clientDetails.setClientId("client2");
			clientDetails.setClientSecret("client2");
			clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
			List<String> ScopeList = new ArrayList<String>();
			ScopeList.add("read");
			ScopeList.add("write");
			ScopeList.add("trust");

			clientDetails.setScope(ScopeList);
			return clientDetails;
		}

		else {
			throw new NoSuchClientException("No client with requested id: "
					+ clientId);
		}
	}
}
