package com.zehntech.matterhorn.service.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailTemplates {

	@Value("${application.name}")
	private String appName;

	/**
	 * Method to design template for active user profile
	 * @param displayName
	 * @param userName
	 * @param url
	 * @return
	 */
	public String addUserEmailTemplate(String displayName, String userName,
			String url) {

		String msgBody = "<div style='color:#646464;font-size:13px;margin:20px;'>"
				+ "<div>"
				+ "<div style='float:left;width:auto;display:inline-block;'>" 
			    + "<img width='50px' src=\"cid:image\">" 
				+ "</div><div style='margin-left:10%;'>Dear <b style='color:#0096c8;'>"
				+ displayName
				+ "</b>,<br><br>"
				+ "Your account has been created on "
				+ appName
				+ "</div></div>"
				+ "<br>"
				+ "Your username is <b>"
				+ userName
				+ "</b>. Please click below to activate your account <br><br>"
				+ "<div style='margin-top:20px;'><a style='text-decoration: none;background-color:#0096c8; color:#fff;display:inline-block;padding: 6px;' href="
				+ url + ">" + "Click to activate your account</a>" + "</div>";

		return msgHearderStart + appName + msgHearderStop + msgBody
				+ msgFooterStart + appName + msgFooterStart1 + appName
				+ msgFooterStop;
	}
	
	
	
	private final String msgHearderStart = "<body>"
			+ "<table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#f2f2f2'>"
			+ "<tr><td>&nbsp;</td></tr>"
			+ "<tr><td align='center'><span style='color:#0096c8;font-size:26px;margin-right:615px;'> ";

	private final String msgHearderStop = " </span></td></tr>"
			+ "<tr><td>&nbsp;</td></tr>"
			+ "<tr><td><table align='center' width='700' border='1px' bgcolor='#ffffff'><tr><td>"
			+ "<div style='color:#646464;font-size:13px;margin:20px;'>";

	private final String msgFooterStart = "<br><hr><span style='float: left;'>Powered by ";

	private final String msgFooterStart1 = " 1.0</span>"
			+ "<a style='color:#0096c8;float:right;text-decoration: none;' target='_blank' href="
			+ "http://www.zehntech.com"
			+ ">Zehntech Technologies<a>"
			+ "</div><br></div>"
			+ "</tr></table>"
			+ "<span style='color:#646464;font-size:13px;margin-left:45%;'>Sent by Zehntech ";

	private final String msgFooterStop = " 1.0</span> "
			+ "<a style='float: right;margin-right:20px;'>"
			+ "<img width='100px' src=\"cid:logo\">" + "<a>"
			+ "</td></tr><tr><td>&nbsp;</td></tr></table></body>";
	
}
