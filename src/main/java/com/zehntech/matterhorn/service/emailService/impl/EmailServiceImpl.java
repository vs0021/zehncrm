package com.zehntech.matterhorn.service.emailService.impl;

import java.io.File;
import java.net.MalformedURLException;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.zehntech.matterhorn.logging.ApplicationLogger;
import com.zehntech.matterhorn.service.email.bean.EmailBean;
import com.zehntech.matterhorn.service.email.impl.EmailService;
import com.zehntech.matterhorn.service.email.impl.SessionCreationException;
import com.zehntech.matterhorn.service.utility.EmailTemplates;

@Service("emailService")
public class EmailServiceImpl implements
com.zehntech.matterhorn.service.emailService.EmailService{

	EmailService emailService = new EmailService();

	@Autowired
	private EmailTemplates emailTemplates;

	ApplicationLogger logger = ApplicationLogger
			.getLogger(EmailServiceImpl.class);

	@Value("${mail.username}")
	private String username;

	@Value("${mail.password}")
	private String password;

	@Value("${mail.smtp.host}")
	private String host;

	@Value("${mail.smtp.protocol}")
	private String protocol;

	@Value("${mail.smtp.port}")
	private String port;

	@Value("${mail.server.protocol}")
	private String serverProtocol;

	@Value("${mail.port}")
	private String serverPort;

	@Value("${mail.server.host}")
	private String serverHost;

	@Value("${mail.smtp.username}")
	private String user;

	@Value("${application.name}")
	private String appName;

	@Autowired
	private ServletContext context;

	private static String path = System.getProperty("catalina.home")
			+ File.separatorChar + "conf";


	public void sendAddUserMail(String senderName, String userName,
			String userEmailId, int userId, String url) {
		logger.debug("Send mail to user for activate account");
		File userImage = null;
		File logo = null;
		String message = emailTemplates.addUserEmailTemplate(userName,
				userEmailId, url);
		EmailBean bean = populateBean(userEmailId, senderName);
		bean.setSubject("Welcome to " + appName
				+ ". Please activate your account");

		/*userImage = new File(context.getRealPath("/app/images"), userId
				+ "_profile_image.png.jpg");*/
		String imgProfilePath = System.getProperty("catalina.home")
				+ File.separator + "webapps" + File.separator + "resources"
				+ File.separator + "profile_images" + File.separator;

		userImage = new File(imgProfilePath,userId + "_profile_image.png");
		
		
		if (!userImage.exists()) {
			userImage = new File(context.getRealPath("/app/images"),
					"thumb.jpg");
		}
		logo = new File(context.getRealPath("/app/images"), "crm.jpg");
		sendMailbyThreadForUsers(bean, message, userImage, logo);
	}

	

	/**
	 * Method to send a mail way a thread
	 * @param bean
	 * @param msg
	 * @param userImg
	 */
	private void sendMailbyThreadForUsers(final EmailBean bean,
			final String msg, final File userImg, final File logo) {

		final String company = ThreadContext.get("COMPANYNAME");
		final String user = ThreadContext.get("USERNAME");
		Runnable run = new Runnable() {
			public void run() {
				try {
					ThreadContext.put("USERNAME", user);
					ThreadContext.put("COMPANYNAME", company);
					logger.debug("sending Email:" + bean.getTo());
					emailService.sendMail(bean, msg, userImg, logo);
				}
				catch (SessionCreationException e) {
					logger.error("Exception in sending email " + e);
				}

				catch (MessagingException e) {
					logger.error("Exception in sending email " + e);

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					
				}
			}
		};
		new Thread(run).start();
	}
	
	
	/**
	 * Method to populate EmailBean
	 * @param to
	 * @param senderName
	 * @return
	 */
	private EmailBean populateBean(String to, String senderName) {

		EmailBean bean = new EmailBean();
		bean.setTo(to);
		bean.setFrom(username);
		bean.setReplyTo(username);
		bean.setPassword(password);
		bean.setUsername(user);
		bean.setMailHost(host);
		bean.setMailTransportProtocol(protocol);
		return bean;
	}
	
}
