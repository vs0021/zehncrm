package com.zehntech.matterhorn.service.emailService;


public interface EmailService {

	void sendAddUserMail(String senderName, String userName,String userEmailId, int userId, String url);
}
