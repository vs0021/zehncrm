package com.zehntech.matterhorn.dao.core.impl;

import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zehntech.matterhorn.dao.core.CoreDao;
import com.zehntech.matterhorn.logging.ApplicationLogger;

@Component("coreDao")
public class CoreDaoImpl implements CoreDao {
	ApplicationLogger logger = ApplicationLogger.getLogger(CoreDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		Session session = null;

		try {
			session = sessionFactory.openSession();
			session.setCacheMode(CacheMode.IGNORE);
			logger.debug("Hibernate session created in UserDaoImpl::");
		}
		catch (HibernateException e) {
			logger.error("HibernateException in creating session in UserDaoImpl"
					+ e);
		}

		return session;
	}

}
