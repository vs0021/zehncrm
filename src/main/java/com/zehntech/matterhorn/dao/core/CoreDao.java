package com.zehntech.matterhorn.dao.core;

import org.hibernate.Session;

public interface CoreDao {
	public Session getSession();

}
