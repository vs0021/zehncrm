package com.zehntech.matterhorn.dao.user;

import java.util.List;

import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.domain.user.AuthUsersData;
import com.zehntech.matterhorn.domain.user.UserRegistration;

public interface UserDao {
	public List<Object> findAuthUsersAndUserDetail(String username);

	CompanyTenantData addCompanyTenant(CompanyTenantData companyTenantData);

	void addUsersData(AuthUsersData authUsersData);

	void updateRegistration(UserRegistration userRegistration);

}
