package com.zehntech.matterhorn.dao.user.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zehntech.matterhorn.dao.core.CoreDao;
import com.zehntech.matterhorn.dao.user.UserDao;
import com.zehntech.matterhorn.domain.companytenant.CompanyTenantData;
import com.zehntech.matterhorn.domain.user.AuthUsersData;
import com.zehntech.matterhorn.domain.user.UserRegistration;
import com.zehntech.matterhorn.logging.ApplicationLogger;

@Component("UserDao")
public class UserDaoImpl implements UserDao {
	ApplicationLogger logger = ApplicationLogger.getLogger(UserDaoImpl.class);

	@Autowired
	CoreDao coreDao;

	public List<Object> findAuthUsersAndUserDetail(String username) {
		String getUser = "from AuthUsersData F left join  F.companyTenantData U   where F.userName=:userName";
		// String getUser = "from AuthUsersData F where F.userName= :userName";
		AuthUsersData authUsersData = null;
		CompanyTenantData companyTenantData = null;
		List<Object> list = null;
		List<Object> userList = new ArrayList<Object>();
		Query query = null;
		Session session = null;
		try {
			session = coreDao.getSession();
			session.beginTransaction();
			logger.info("Fetching AuthUsersData details from AUT_USERS TABLE::"
					+ getUser);
			query = session.createQuery(getUser);
			query.setParameter("userName", username);

			// authUsersData = (AuthUsersData) query.uniqueResult();
			list = query.list();
			for (Object object : list) {
				Object[] list1 = (Object[]) object;
				authUsersData = (AuthUsersData) list1[0];
				companyTenantData = (CompanyTenantData) list1[1];
				userList.add(authUsersData);
				userList.add(companyTenantData);
			}
			/*
			 * if (authUsersData != null) {
			 * authUsersData.setCompanyTenantData(authUsersData
			 * .getCompanyTenantData()); }
			 */

		}
		catch (Exception e) {
			logger.error("HibernateException in getting user detail from AUTH_USERS table by user name"
					+ e);

		}
		finally {
			session.flush();
			session.close();
			logger.debug("Session flush in findAuthUsersDataByName method");
		}
		return userList;
	}

	public CompanyTenantData addCompanyTenant(
			CompanyTenantData companyTenantData) {
		Session session = null;
		CompanyTenantData companyTenant = null;
		try {
			session = coreDao.getSession();
			session.beginTransaction();

			companyTenant = (CompanyTenantData) session
					.merge(companyTenantData);

			logger.debug("Adding commpany tenant information in the table COMPANY_TENANT");
			session.getTransaction().commit();
		}
		catch (HibernateException e) {
			logger.error("HibernateException in adding commpany tenant information in the table COMPANY_TENANT"
					+ e);

		}
		finally {
			session.close();
			logger.debug("Session close in addCompanyTenant method");
		}
		return companyTenant;
	}

	public void addUsersData(AuthUsersData authUsersData) {
		Session session = null;
		try {
			session = coreDao.getSession();
			session.beginTransaction();

			session.saveOrUpdate(authUsersData);

			logger.debug("User data are inserted in the AUTH_USERS,AUTH_AUTHORITIES and USER_DETAIL table");

			session.getTransaction().commit();

		}

		catch (HibernateException e) {
			logger.error("HibernateException in adding user details in the database:"
					+ e);

		}
		finally {

			session.close();
			logger.debug("Session close in addUsersData method");
		}
	}

	public void updateRegistration(UserRegistration userRegistration) {
		Session session = null;
		try {
			session = coreDao.getSession();
			session.beginTransaction();
			session.merge(userRegistration);
			logger.debug("User data are updated in the AUTH_USERS table");
			session.getTransaction().commit();
		}
		/*
		 * catch (NonUniqueObjectException e) { session.merge(authUsersData);
		 * session.getTransaction().commit(); }
		 */
		catch (HibernateException e) {
			logger.error("HibernateException in editing user details in the database:"
					+ e);

		}
		finally {
			session.close();
			logger.debug("Session close in editAuthUsersData method");
		}
	}

}
