package com.zehntech.matterhorn.bean.forms;

import java.sql.Timestamp;

public class UserDetailForm {
	private int userId;

	private String login;

	private String password;

	private String roles[];

	private String groups[];

	private String userName;

	private Boolean notify;

	private int passwordGenerateType;

	private Boolean passwordGenerate;

	private int userStatus;

	private String timeZone;

	private String language;

	private String phoneNumber;

	private String dob;

	private Timestamp createdDate;

	private Timestamp validFrom;

	private Timestamp validUntil;

	private Timestamp updatedDate;

	private float storage;

	private boolean adminFlag;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Boolean getNotify() {
		return notify;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public void setNotify(Boolean notify) {
		this.notify = notify;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public String getDisplayName() {
		return userName;
	}

	public void setDisplayName(String displayName) {
		this.userName = displayName;
	}

	public Boolean getPasswordGenerate() {
		return passwordGenerate;
	}

	public void setPasswordGenerate(Boolean passwordGenerate) {
		this.passwordGenerate = passwordGenerate;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String date) {
		this.dob = date;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Timestamp validFrom) {
		this.validFrom = validFrom;
	}

	public Timestamp getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Timestamp validUntil) {
		this.validUntil = validUntil;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String[] getGroups() {
		return groups;
	}

	public void setGroups(String[] groups) {
		this.groups = groups;
	}

	public int getPasswordGenerateType() {
		return passwordGenerateType;
	}

	public void setPasswordGenerateType(int passwordGenerateType) {
		this.passwordGenerateType = passwordGenerateType;
	}

	public float getStorage() {
		return storage;
	}

	public void setStorage(float storage) {
		this.storage = storage;
	}

	public boolean isAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(boolean adminFlag) {
		this.adminFlag = adminFlag;
	}

}
