package com.zehntech.matterhorn.bean.forms;

import java.util.List;

public class CompanyTenantForm {

	public int companyTenantId;

	public String companyName;

	public String webSite;

	public Boolean notify;

	public String address;

	public String zipCode;

	public String city;

	public String countryName;

	public String countryCode;

	public String stateName;

	public String stateCode;

	public int numberOfUser = 0;

	public int remainingUser = 0;

	public int storageSpace;

	public String expiryDate;

	public boolean status;

	public String tenantIDModal;

	public String folderPath;

	public String remainingSpace;

	public String getTenantIDModal() {
		return tenantIDModal;
	}

	public void setTenantIDModal(String tenantIDModal) {
		this.tenantIDModal = tenantIDModal;
	}

	public List<CompanyAdminDetailForm> companyAdminDetailForm;

	public int getCompanyTenantId() {
		return companyTenantId;
	}

	public void setCompanyTenantId(int companyTenantId) {
		this.companyTenantId = companyTenantId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public Boolean getNotify() {
		return notify;
	}

	public void setNotify(Boolean notify) {
		this.notify = notify;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public int getNumberOfUser() {
		return numberOfUser;
	}

	public void setNumberOfUser(int numberOfUser) {
		this.numberOfUser = numberOfUser;
	}

	public int getStorageSpace() {
		return storageSpace;
	}

	public void setStorageSpace(int storageSpace) {
		this.storageSpace = storageSpace;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public List<CompanyAdminDetailForm> getCompanyAdminDetailForm() {
		return companyAdminDetailForm;
	}

	public void setCompanyAdminDetailForm(
			List<CompanyAdminDetailForm> companyAdminDetailForm) {
		this.companyAdminDetailForm = companyAdminDetailForm;
	}

	public int getRemainingUser() {
		return remainingUser;
	}

	public void setRemainingUser(int remainingUser) {
		this.remainingUser = remainingUser;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getRemainingSpace() {
		return remainingSpace;
	}

	public void setRemainingSpace(String remainingSpace) {
		this.remainingSpace = remainingSpace;
	}

}
